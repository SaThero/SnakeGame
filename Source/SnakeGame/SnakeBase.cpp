// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	NextMoveDirection = LastMoveDirection = EMovementDirection::UP;
	ElementsToSpawn = ElementsCount = 4 ;
	FoodToSpawn = 2;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	SpawnFood(FoodToSpawn);

	AddSnakeElement(ElementsToSpawn);

}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}


void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	FVector BaseLocation(ForceInitToZero);
	FRotator BaseRotation(ForceInitToZero);
	switch (NextMoveDirection)
	{
	case EMovementDirection::UP:
		BaseLocation.X -= ElementSize;
		BaseRotation.Yaw = 270;
		break;
	case EMovementDirection::DOWN:
		BaseLocation.X += ElementSize;
		BaseRotation.Yaw = 90;
		break;
	case EMovementDirection::RIGHT:
		BaseLocation.Y -= ElementSize;
		BaseRotation.Yaw = 0;
		break;
	case EMovementDirection::LEFT:
		BaseLocation.Y += ElementSize;
		BaseRotation.Yaw = 180;
		break;
	}

	FVector NewLocation(ForceInitToZero);
	for (int i = 0; i < ElementsNum; i++)
	{
		NewLocation = BaseLocation*SnakeElements.Num();
		FTransform NewTransform(BaseRotation,NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
		ElementsToSpawn--;
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector (ForceInitToZero);
	FRotator MovementRotation(ForceInitToZero);
	FRotator SecondaryRotation(ForceInitToZero);
	FVector SecondaryOffset(ForceInitToZero);
	switch (NextMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		SecondaryRotation.Yaw = MovementRotation.Yaw = 270;
		switch (LastMoveDirection)
		{
		case EMovementDirection::RIGHT:
			SecondaryRotation.Yaw = 315;
			SecondaryOffset.X += ElementSize / 3.3;
			SecondaryOffset.Y -= ElementSize / 3.3;
			break;
		case EMovementDirection::LEFT:
			SecondaryRotation.Yaw = 225;
			SecondaryOffset.X += ElementSize / 3.3;
			SecondaryOffset.Y += ElementSize / 3.3;
			break;
		}	
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		SecondaryRotation.Yaw=MovementRotation.Yaw = 90;
		switch (LastMoveDirection)
		{
		case EMovementDirection::RIGHT:
			SecondaryRotation.Yaw = 45;
			SecondaryOffset.X -= ElementSize / 3.3;
			SecondaryOffset.Y -= ElementSize / 3.3;
			break;
		case EMovementDirection::LEFT:
			SecondaryRotation.Yaw = 135;
			SecondaryOffset.X -= ElementSize / 3.3;
			SecondaryOffset.Y += ElementSize / 3.3;

			break;
		}
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementSize;
		SecondaryRotation.Yaw = MovementRotation.Yaw = 0;
		switch (LastMoveDirection)
		{
		case EMovementDirection::UP:
			SecondaryRotation.Yaw = 315;
			SecondaryOffset.X -= ElementSize / 3;
			SecondaryOffset.Y += ElementSize / 3;
			break;
		case EMovementDirection::DOWN:
			SecondaryRotation.Yaw = 45;
			SecondaryOffset.X += ElementSize / 3;
			SecondaryOffset.Y += ElementSize / 3;
			break;
		}
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementSize;
		SecondaryRotation.Yaw = MovementRotation.Yaw = 180;
		switch (LastMoveDirection)
		{
		case EMovementDirection::UP:
			SecondaryRotation.Yaw = 225;
			SecondaryOffset.X -= ElementSize / 3;
			SecondaryOffset.Y -= ElementSize / 3;
			break;
		case EMovementDirection::DOWN:
			SecondaryRotation.Yaw = 135;
			SecondaryOffset.X += ElementSize / 3;
			SecondaryOffset.Y -= ElementSize / 3;
			break;
		}
		break;
	}
	LastMoveDirection = NextMoveDirection;

	
	SnakeElements[0]->ToggleCollision();
	//remember where head was and move it
	FTransform PrevLocation = SnakeElements[0]->GetTransform();
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->SetActorRotation(MovementRotation);
	//move last element where head was or spawn there new element if needed
	ASnakeElementBase* SecondSnakeElem;
	if (ElementsToSpawn > 0)
	{
		SecondSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass,SnakeElements.Last()->GetActorTransform());
		SnakeElements.Add(SecondSnakeElem); //as we can't really add new elem to table right now but need to count it
		ElementsToSpawn--;
	} 
	
		SecondSnakeElem =SnakeElements.Last();
		SecondSnakeElem->SetActorTransform(PrevLocation);
	
	// Update all SnakeElement reference so they still in right order here we get optimization by not moving all the snake
	int SnakeSize = SnakeElements.Num();
	for (int i =  SnakeSize - 1; i > 1; i--)
	{
		SnakeElements[i] = (SnakeElements[i - 1]);
	}
	SnakeElements[1]=SecondSnakeElem; 
	SnakeElements[1]->AddActorWorldOffset(SecondaryOffset);
	SnakeElements[1]->SetActorRotation(SecondaryRotation);
	SnakeElements[0]->ToggleCollision();
	// making Last element smaler for fun
	SnakeElements.Last()->SetActorScale3D(FVector(.3, .7, .3));

	//spawn food while not messing with snake collisions so food hopefully never spawn inside snake
	SpawnFood(FoodToSpawn);
	
	
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this,bIsFirst);
		}
	}
}

void ASnakeBase::SpawnFood(int N)
{
	for (int i = 0; i < N; i++)
	{
		// spawn food at random location untill it spawns correctly, note to enshure game ends before all space was occupied. or add some counter to avoid infinite cycle.
		bool b = true;
		FVector Position(ForceInitToZero);
		Position.X = FMath::RandRange(((int)(GridSize / 2)) - GridSize, (int)(GridSize / 2)) * ElementSize;
		Position.Y = FMath::RandRange(((int)(GridSize / 2)) - GridSize, (int)(GridSize / 2)) *ElementSize;
		AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, FTransform(Position));
		if (NewFood) { FoodToSpawn--; }
	}
}
